#! /bin/bash

die () {
    echo $1
    exit 1
}

if [ $UID != 0 ]; then
    echo "This script must be run as root!"
    exit 1
fi

echo "Please enter your WUSTL ID: "
read WUSTLID
echo "Please enter your WUSTL Key: "
read -s WUSTLKEY

mkdir -p /var/vpn
                                            
apt-get -y install build-essential \
    libreadline-dev libssl-dev libncurses5-dev \
    lib32z1-dev net-tools 2> /var/vpn/apt-get.error || die "Failed to install packages.  See /var/vpn/apt-get.error."
    
cd /opt

if [ -d SoftEtherVPN_Stable ]; then
    cd SoftEtherVPN_Stable
    git pull || \
      die "SoftEther VPN Git repo is not useable.  Please delete /opt/SoftEtherVPN_Stable and run this script again."
else
    echo
    echo "Cloning SoftEther VPN repo...  This might take some time depending on your internet connection."
    git clone --depth 1 https://github.com/SoftEtherVPN/SoftEtherVPN_Stable.git 1>/var/vpn/git.output 2> /var/vpn/git.error || \
      die "Could not clone SoftEtherVPN_Stable repository.  See /var/vpn/git.error"
fi

cd SoftEtherVPN_Stable
echo
echo "Configuring SoftEther VPN..."
./configure 1> /var/vpn/configure.output 2> /var/vpn/configure.error || \
  die "Failed to configure.  See /var/vpn/configure.error"
echo
echo "Compiling and installing SoftEther VPN.   This will take some time." 
make 1> /var/vpn/make.output 2> /var/vpn/make.error || \
  die "Failed to build SoftEther VPN.  See /var/vpn/make.error"
make install || die "Failed to install SoftEther VPN"

echo
echo "Installation complete."
echo 
echo "Configuring client..."

cat << 'VPNCLIENT' > /etc/systemd/system/softether-vpnclient.service
[Unit]
Description=SoftEther VPN Client
After=network.target auditd.service
ConditionPathExists=!/usr/vpnclient/do_not_run

[Service]
Type=forking
EnvironmentFile=-/usr/vpnclient
ExecStart=/usr/vpnclient/vpnclient start
ExecStop=/usr/vpnclient/vpnclient stop
KillMode=process
Restart=on-failure

# Hardening
PrivateTmp=yes
ProtectHome=yes
ProtectSystem=full
ReadOnlyDirectories=/
ReadWriteDirectories=-/usr/vpnclient
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_BROADCAST CAP_NET_RAW CAP_SYS_NICE CAP_SYS_ADMIN CAP_SETUID

[Install]
WantedBy=multi-user.target
VPNCLIENT

cat << NRGVPN > /tmp/NRG.vpn
# VPN Client VPN Connection Setting File
# 
# This file is exported using the VPN Client Manager.
# The contents of this file can be edited using a text editor.
# 
# When this file is imported to the Client Connection Manager
#  it can be used immediately.

declare root
{
	bool CheckServerCert false
	uint64 CreateDateTime 0
	uint64 LastConnectDateTime 0
	bool StartupAccount false
	uint64 UpdateDateTime 0

	declare ClientAuth
	{
		uint AuthType 2
		byte EncryptedPassword \$
		string Username \$
	}
	declare ClientOption
	{
		string AccountName NRG
		uint AdditionalConnectionInterval 1
		uint ConnectionDisconnectSpan 0
		string DeviceName se
		bool DisableQoS false
		bool HalfConnection true
		bool HideNicInfoWindow false
		bool HideStatusWindow false
		string Hostname vpn.nrg.wustl.edu
		string HubName NRG
		uint MaxConnection 16
		bool NoRoutingTracking false
		bool NoTls1 false
		bool NoUdpAcceleration false
		uint NumRetry 4294967295
		uint Port 443
		uint PortUDP 0
		string ProxyName \$
		byte ProxyPassword \$
		uint ProxyPort 0
		uint ProxyType 0
		string ProxyUsername \$
		bool RequireBridgeRoutingMode false
		bool RequireMonitorMode false
		uint RetryInterval 15
		bool UseCompress false
		bool UseEncrypt true
	}
}
NRGVPN

cat << CACERT > /tmp/NRG.crt
-----BEGIN CERTIFICATE-----
MIIGXjCCBEagAwIBAgIBADANBgkqhkiG9w0BAQsFADCBrTEaMBgGA1UEAwwRdnBu
Lm5yZy53dXN0bC5lZHUxMTAvBgNVBAoMKFdhc2hpbmd0b24gVW5pdmVyc2l0eSBT
Y2hvb2wgb2YgTWVkaWNpbmUxKDAmBgNVBAsMH05ldXJvaW5mb3JtYXRpY3MgUmVz
ZWFyY2ggR3JvdXAxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhNaXNzb3VyaTESMBAG
A1UEBwwJU3QuIExvdWlzMB4XDTE4MDkwNjE0MDMxN1oXDTI4MDkwMzE0MDMxN1ow
ga0xGjAYBgNVBAMMEXZwbi5ucmcud3VzdGwuZWR1MTEwLwYDVQQKDChXYXNoaW5n
dG9uIFVuaXZlcnNpdHkgU2Nob29sIG9mIE1lZGljaW5lMSgwJgYDVQQLDB9OZXVy
b2luZm9ybWF0aWNzIFJlc2VhcmNoIEdyb3VwMQswCQYDVQQGEwJVUzERMA8GA1UE
CAwITWlzc291cmkxEjAQBgNVBAcMCVN0LiBMb3VpczCCAiIwDQYJKoZIhvcNAQEB
BQADggIPADCCAgoCggIBAKhIuMKqYT6uxXnk83YEpuMPEE/eQLTEGxtfdRZY/Byq
RKxzLeqMc/7kIDtdVSLuNCuiHL2Uah2g2UQ4PJtLdjO4AXjUmgJO1JQR15FtjNUx
/Ak2G6OkTFekYNRbN96b7YD5sm8l0S+wvqa341nF7eP2nwITKtZ+ksGF6xIsUEr7
Z/6xVKYe+mPNlzFMDLHoVnjvxYInstWOQjnwGrC/b63aj+6ZEqdYr8Mj2ma04FuE
qbpjaoRqY2uCYjN4CYdW40zRhg4XTe4z5hNKLWYm0DtLZofoZnmV1YfeNqYnFlXm
pp4QMsFrxa311a+pWtt/ZckWfhGUoeiDDtIxE2Y8EUaCsAo1Iu7ctz3QnlJuB0xs
DBvhEQQOLbqArum8P+Ui0A40ef0SH5k30usigK6EDljKsSaoXByKMP3CCGS7KABQ
I5KIjgs3arcLzkDBr0CAlNcjNpNw4Ws84mjb2N+yFdv1kX+abGXtn/PA7br7cQQX
BMJASRE7C+20oNKGJoT8jqN2+D7PnhDWhs+YcOC4mZU+bJ+FCOK13ui9DnAD3SxR
wz9PkzUMNlxTIMDJ70e6r5RGJXI6IjEi2ti9LlRq1L0mWi8HSGwjJnd+m8A0kyD9
Mh2UYIyCcF+ED0K3cdP4xqC2cwdB0GOJpmeO0zIN47uCMVB3vxe3qBbdz8ZoM/yx
AgMBAAGjgYYwgYMwDwYDVR0TAQH/BAUwAwEB/zALBgNVHQ8EBAMCAfYwYwYDVR0l
BFwwWgYIKwYBBQUHAwEGCCsGAQUFBwMCBggrBgEFBQcDAwYIKwYBBQUHAwQGCCsG
AQUFBwMFBggrBgEFBQcDBgYIKwYBBQUHAwcGCCsGAQUFBwMIBggrBgEFBQcDCTAN
BgkqhkiG9w0BAQsFAAOCAgEABndnXPxP2bqA5ur5wgrzjUZSU3onX1yRCciBg5+r
hoU5MWWSAj2osEq4mvLczBInS3V0B9Zq2vFe0FdGkd9Nq0q1QMvTrvGPh/hXcxvB
UcvBkKE9447ajINCwCdMo9bH4x8yg/sc0MdUw5sL6WFRpbTUxdunJuFxuzONviwO
d5oAwDoPuM4acBqHZjv/VB539GjjmY8RHl2ogAcj2n/k3RpwY+D8PbsBOpLkpUuW
arjViJbX9LhdSaOUPKzV5l9EAW8lwIaP0cJr4LrTeSZJNEa4II0L64YDU1kbXE4s
IsiTSCDG+JbXfCiB+Yr+aK2I2XPLPIFyCyOKLze1I6RQiEMGzXhEKnWnohbiFrcb
gaJckZUa2cCAl/z/ZPUODEHRWG0xjIyAY48rsrKsiAte0j0vxTZaEp84TYywwRlf
fMglg/gFXaUSViqoJTOBUVi0Sh9F1/Afz7kgMEk0yFw8LD+NJbU9nJwhVyUwXleY
iGfS+tZPOySpdTTgfjJUac5xzzefaHpJ4j/1couLfMDT+HVondABMy6+/SRbRCUn
kPoGrCO8/ZlaexSF6pDXAxhRfxjcqmCn8f4zVhIcnLET8vj6ojgKfZoyd9/CbZHi
bKKcqRsyc0QIllwTEEyyUdMOTUGjwH1zxx8Ew0aSD+TBwZJNFFuq87JmTffL1BFq
HFc=
-----END CERTIFICATE-----
CACERT

cat << 'SETUPNRG' > /tmp/setupNRG
NicCreate se
NicEnable se
AccountDelete NRG
AccountImport 
AccountUsernameSet NRG 
AccountPasswordSet NRG /TYPE:radius
CertAdd
AccountList
CertList
SETUPNRG

systemctl enable softether-vpnclient.service
systemctl start softether-vpnclient.service

echo -e "/tmp/NRG.vpn\n${WUSTLID}\n${WUSTLKEY}\n${WUSTLKEY}\n/tmp/NRG.crt\n" | vpncmd localhost /CLIENT /IN:/tmp/setupNRG  

cat /etc/network/interfaces | grep -q 'auto-hotplug vpn_se' || \
  echo >> /etc/network/interfaces && \
  echo "auto-hotplug vpn_se" >> /etc/network/interfaces && \
  echo "iface vpn_se inet dhcp" >> /etc/network/interfaces && \
  echo "pre-up vpncmd localhost /CLIENT /IN:/var/vpn/vpn.up" >> /etc/network/interfaces && \
  echo "post-down vpncmd localhost /CLIENT /IN:/var/vpn/vpn.down" >> /etc/network/interfaces
  
  
cat << 'ENABLEVPN' > /var/vpn/vpn.up
AccountConnect NRG
AccountList
ENABLEVPN

cat << 'DISABLEVPN' >/var/vpn/vpn.down
AccountDisconnect NRG
AccountList
DISABLEVPN

echo
echo "SoftEther VPN is installed and configured to connect to the NRG network."
echo
echo "To start: sudo ifup vpn_se"
echo "To stop: sudo ifdown vpn_se"


 
